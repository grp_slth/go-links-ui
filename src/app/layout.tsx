import { Roboto } from 'next/font/google'
import type { Metadata } from 'next'
import ThemeRegistry from './themeRegistry'
import '../styles/globals.css'

const roboto = Roboto({ 
  weight: "400",
  style: "normal",
  subsets: ["cyrillic"]
})

export const metadata: Metadata = {
  title: process.env.APP_NAME,
  description: process.env.APP_DESCRIPTION,
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={roboto.className}>
        <ThemeRegistry options={{key: "golinks"}}>
          {children}
        </ThemeRegistry>
      </body>
    </html>
  )
}
