import React from "react";
import {getList} from '../lib/linkService'
import Navigation from "../components/navbar"
import LinkTable from "../components/table";
import {links} from "../lib/mock-data"
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"
import { AspectRatio } from "@/components/ui/aspect-ratio"
import { DeleteIcon } from "components/icons/deleteIcon";
import { EditIcon } from "components/icons/editIcon";
import Image from "next/image";
import pageImg from '../lib/mock-page-image.png'

type LinkProp = { 
  id: number, 
  goLink: string, 
  redirectURL: string
}

export default async function Home() {
  // const linkResponse = await getList()

  // const linkResponse = links;
  return (
    <div>
      <Navigation /> 
      <main className="flex flex-col items-center justify-between pt-6 p-12 sticky">
        {links.map((link: LinkProp) => (
            <Card key={link.id}>
              <CardContent>
                <AspectRatio ratio={16 / 9}>
                  <Image src={pageImg} alt=""/>
                </AspectRatio>
              </CardContent>
              <CardHeader>
                <CardTitle>fflogs for Robo Bean</CardTitle>
                <CardDescription>fflogs page for character Robo Bean</CardDescription>
              </CardHeader>
              <CardFooter>
                <DeleteIcon />
                <EditIcon />
              </CardFooter>
            </Card>
        ))}
        {/* <LinkTable links={linkResponse} />  */}
      </main>
    </div>
  )
}