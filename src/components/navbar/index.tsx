'use client';
import React from "react";
import Logo from "./logo";
import {Box, Button, IconButton, InputAdornment, TextField, Typography} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import Modal from '@mui/material/Modal';
import { useRouter } from 'next/navigation';


const buttonSX = {
    backgroundColor: "#2279A7 !important",
    marginTop: ".5rem",
    marginRight: "1.5rem",
    "&:hover": {
        backgroundColor: "#2B96D0 !important"
    },
    fontSize: 17,
}

const searchSX = {
    marginTop: ".5rem",
    width: "60rem",
    fontSize: 17
}

const modalSX = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

async function handleSavingLink(goLink: string, url: string) {
    const body = {
        "goLink": goLink,
        "redirectURL": url
    }
    const response = await fetch("https://api.local.marcelosalinas.dev/golinks/", {
        method: "PUT",
        headers: {
        "Content-Type": "application/json",
        },
        body: JSON.stringify(body), 
    })

    if (!response.ok) {
        throw new Error ("Failed to save link details")
    }
}

const Navbar = () => {
    const router = useRouter();
    const [open, setOpen] = React.useState(false)
    const [goLink, setGoLink] = React.useState("")
    const [url, setURL] = React.useState("")

    return (
            <div className="w-full h-[4.5rem] navbar sticky top-0">
                <div className="flex justify-between items-center h-full mt-2">
                    <div className="flex items-center h-full">
                        <Logo /> 
                        <p className="pl-4 pt-2 text-3xl text-[#2279A7] font-extrabold">Go To</p>
                    </div>
                    <div className="flex search-field">
                        <TextField
                            sx={searchSX}
                            InputLabelProps={{style: {color : '#EDEDF3'}}}
                            variant="outlined"
                            label="Search"
                            InputProps={{
                                endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton>
                                        <SearchIcon />
                                    </IconButton>
                                </InputAdornment>
                                )
                            }}
                        />
                    </div>
                    <Button sx={buttonSX} variant="contained" onClick={() => setOpen(true)}>Create Link</Button>
                </div>
                <Modal
                    open={open}
                    onClose={() => setOpen(false)}
                    aria-labelledby="modal-create-link-modal"
                    aria-describedby="modal-create-a-new-link"
                >
                    <Box sx={modalSX}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Create a new GoLink
                        </Typography>
                            <TextField
                                sx={{
                                    marginTop: "1rem",
                                    width: "100%",
                                    fontSize: 17
                                }}
                                InputLabelProps={{style: {color : '#EDEDF3'}}}
                                variant="outlined"
                                label="URL"
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                    setURL(event.target.value)
                                }}
                            />
                            <TextField
                                sx={{
                                    marginTop: "1rem",
                                    width: "100%",
                                    fontSize: 17
                                }}
                                InputLabelProps={{style: {color : '#EDEDF3'}}}
                                variant="outlined"
                                label="Short Name"
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                    setGoLink(event.target.value)
                                }}
                            />
                        <Button sx={{
                            backgroundColor: "#2279A7 !important",
                            marginTop: "1rem",
                            "&:hover": {
                                backgroundColor: "#2B96D0 !important"
                            },
                            fontSize: 17,                        
                        }} 
                        variant="contained" 
                        onClick={async () => {
                            await handleSavingLink(goLink, url)
                            setOpen(false)
                            router.refresh();
                        }}>
                            Save
                        </Button>
                    </Box>
                </Modal>
            </div>
    );
};

export default Navbar;