"use client";
import Image from "next/image";
import { useEffect, useState } from "react";

const Logo = () => {
    //update the size of the logo when the size of the screen changes
    const [width, setWidth] = useState(0);

    const updateWidth = () => {
        const newWidth = window.innerWidth;
        setWidth(newWidth);
    };

    useEffect(() => {
        window.addEventListener("resize", updateWidth);
        updateWidth();
    }, []);

    return (
                <Image
                    src="/images/logo.png" //TODO: add env variable for image link
                    alt="Logo"
                    width={width < 1024 ? "50" : "50"} //TODO: add env variables for size, if large doesn't exist use default for both
                    height={width < 1024 ? "50" : "50"} //TODO: add env variables for size, if large doesn't exist use default for both
                    className="logo"
                />
    );
};

export default Logo;