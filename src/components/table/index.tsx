'use client';
import React, {Suspense} from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import EditIcon  from '@mui/icons-material/Edit';
import DeleteIcon from "@mui/icons-material/Delete";
import { columns } from "../../lib/mock-data";
import { IconButton, Typography } from "@mui/material";

type LinkProp = { 
    id: number, 
    goLink: string, 
    redirectURL: string
}

type TableProps = {
    links: Array<LinkProp>
}

const TableHeadSX = { 
    backgroundColor: "#161A1E !important",
}

async function handleDeletingLink(id: number) {
    const response = await fetch(`https://api.local.marcelosalinas.dev/golinks/${id}`, {
        method: "DELETE",
        headers: {
        "Content-Type": "application/json",
        } 
    })

    if (!response.ok) {
        throw new Error ("Failed to save link details")
    }
}

const LinkTable = ({links} : TableProps) => {
    return (
        <Suspense>
            <TableContainer component={Paper} className="link-table">
                <Table aria-label="simple table" stickyHeader >
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell align={"center"} key={column.name} sx={TableHeadSX} >
                                    <Typography variant="subtitle1">{column.name}</Typography>
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {links.map((link: LinkProp) => (
                            <TableRow
                                key={link.id}
                                hover={true}
                            >
                                <TableCell>
                                    {link.goLink}
                                </TableCell>
                                <TableCell >
                                    {/* <div className="text-left"> */}
                                        {link.redirectURL}
                                    {/* </div> */}
                                </TableCell>
                                <TableCell align="center">
                                    <IconButton aria-label="edit" >
                                        <EditIcon />
                                    </IconButton>
                                    <IconButton aria-label="delete" color="error" onClick={(a) => {
                                        handleDeletingLink(link.id)
                                    }}>
                                        <DeleteIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Suspense>
    );
};

export default LinkTable;
