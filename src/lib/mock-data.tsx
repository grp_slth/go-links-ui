export const columns = [
    { name: "Go Link"},
    { name: "Redirect URL"},
    { name: "Actions"},
];

export const links = [
    {
        id: 1,
        goLink: "portainer",
        redirectURL: "portainer.local.marcelosalinas.dev",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 2,
        goLink: "dashboard",
        redirectURL: "https://homarr.local.marcelosalinas.dev",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 3,
        goLink: "traefik",
        redirectURL: "https://traefik.local.marcelosalinas.dev",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 4,
        goLink: "wiki",
        redirectURL: "https://wiki.local.marcelosalinas.dev",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 5,
        goLink: "proxmox",
        redirectURL: "https://proxmox.local.marcelosalinas.dev",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 6,
        goLink: "gitlab",
        redirectURL: "https://gitlab.local.marcelosalinas.dev",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 7,
        goLink: "pihole",
        redirectURL: "http://pi.hole/admin",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 8,
        goLink: "router",
        redirectURL: "https://routerlogin.net/start.htm",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 9,
        goLink: "router2",
        redirectURL: "https://routerlogin.net/start.htm",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 10,
        goLink: "router3",
        redirectURL: "https://routerlogin.net/start.htm",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 11,
        goLink: "router4",
        redirectURL: "https://routerlogin.net/start.htm",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 12,
        goLink: "router5",
        redirectURL: "https://routerlogin.net/start.htm",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 13,
        goLink: "router6",
        redirectURL: "https://routerlogin.net/start.htm",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 14,
        goLink: "router7",
        redirectURL: "https://routerlogin.net/start.htm",
        creationTimestamp: "2023-10-21 23:57:54",
    },
    {
        id: 15,
        goLink: "router8",
        redirectURL: "https://routerlogin.net/start.htm",
        creationTimestamp: "2023-10-21 23:57:54",
    },

];

