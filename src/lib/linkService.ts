export async function getList() {
    const response = await fetch("https://api.local.marcelosalinas.dev/golinks/", { cache: 'no-store' })

    if (!response.ok) {
        throw new Error("Failed to get links")
    }

    return response.json()
}